﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using Data.Entities;
using Data.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ReaderRepository : Repository<Reader>, IReaderRepository
    {
        private readonly LibraryDbContext _libraryDbContext;
        public ReaderRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
            _libraryDbContext = libraryDbContext;
        }
        public IQueryable<Reader> GetAllWithDetails()
        {
            return _libraryDbContext.Readers.Include(r => r.ReaderProfile)
                .Include(r => r.Cards);
        }

        public Task<Reader> GetByIdWithDetails(int id)
        {
            return GetAllWithDetails().FirstOrDefaultAsync(r => r.Id == id);
        }
    }
}
