﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class CardRepository : Repository<Card>, ICardRepository
    {
        private readonly LibraryDbContext _libraryDbContext;
        public CardRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
            _libraryDbContext = libraryDbContext;
        }
        public IQueryable<Card> FindAllWithDetails()
        {
            return _libraryDbContext.Cards.Include(c => c.Books)
                .Include(c => c.Reader);
        }

        public Task<Card> GetByIdWithDetailsAsync(int id)
        {
            return FindAllWithDetails().FirstOrDefaultAsync(c => c.Id == id);
        }
    }
}
