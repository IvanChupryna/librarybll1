﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class BookRepository : Repository<Book>, IBookRepository
    {
        private readonly LibraryDbContext _libraryDbContext;
        public BookRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext) 
        {
            _libraryDbContext = libraryDbContext;
        }

        public IQueryable<Book> FindAllWithDetails()
        {
            return _libraryDbContext.Books.Include(b => b.Cards);
        }

        public Task<Book> GetByIdWithDetailsAsync(int id)
        {
            return FindAllWithDetails().FirstOrDefaultAsync(b => b.Id == id);
        }
    }
}
