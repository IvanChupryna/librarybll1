﻿using Data.Entities.Abstract;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly DbSet<TEntity> _entities;
        public Repository(DbContext dbContext)
        {
            _entities = dbContext.Set<TEntity>();
        }
        public Task AddAsync(TEntity entity)
        {
            return _entities.AddAsync(entity).AsTask();
        }

        public void Delete(TEntity entity)
        {
            _entities.Remove(entity);
        }

        public Task DeleteByIdAsync(int id)
        {
            Task<TEntity> t = GetByIdAsync(id);
            _entities.Remove(t.Result);
            return t;
        }

        public IQueryable<TEntity> FindAll()
        {
            return _entities;
        }

        public Task<TEntity> GetByIdAsync(int id)
        {
            return _entities.FindAsync(id).AsTask();
        }

        public void Update(TEntity entity)
        {
            _entities.Update(entity);
        }
    }
}
