﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using Data.Entities;
using Data.Interfaces;
using System.Linq;

namespace Data.Repositories
{
    public class HistoryRepository : Repository<History>, IHistoryRepository
    {
        private readonly LibraryDbContext _libraryDbContext;
        public HistoryRepository(LibraryDbContext libraryDbContext) : base(libraryDbContext)
        {
            _libraryDbContext = libraryDbContext;
        }
        public IQueryable<History> GetAllWithDetails()
        {
            return _libraryDbContext.Histories.Include(h => h.Book)
                .Include(h => h.Card);
        }
    }
}
