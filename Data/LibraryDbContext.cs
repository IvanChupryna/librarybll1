using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class LibraryDbContext : DbContext
    {
        public DbSet<ReaderProfile> ReaderProfiles { get; set; }
        public DbSet<Reader> Readers { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<Book> Books { get; set; }

        public LibraryDbContext() : base() { }
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options) : base(options)
        {
        }
    }
}