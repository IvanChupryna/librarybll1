﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities.Abstract
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
