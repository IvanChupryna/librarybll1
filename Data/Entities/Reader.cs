﻿using Data.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Reader : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
        public virtual ReaderProfile ReaderProfile { get; set; }
    }
}
