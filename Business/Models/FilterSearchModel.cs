﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class FilterSearchModel
    {
        public string Author { get; set; }
        public int Year { get; set; }
    }
}
