using AutoMapper;
using Business.Models;
using Data.Entities;
using System.Linq;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Book, BookModel>()
                .ForMember(p => p.CardsIds, c => c.MapFrom(card => card.Cards.Select(x => x.CardId)))
                .ReverseMap();

            CreateMap<Reader, ReaderModel>()
                .ForMember(r => r.Phone, p => p.MapFrom(phone => phone.ReaderProfile.Phone))
                .ForMember(r => r.Address, a => a.MapFrom(address => address.ReaderProfile.Address))
                .ForMember(r => r.CardsIds, c => c.MapFrom(card => card.Cards.Select(x => x.Id)))
                .ReverseMap();

            CreateMap<Card, CardModel>()
                .ForMember(c => c.BooksIds, b => b.MapFrom(book => book.Books.Select(x => x.BookId)))
                .ReverseMap();
        }
    }
}