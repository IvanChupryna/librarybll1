﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class StatisticService : IStatisticService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly Mapper _mapper;

        public StatisticService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<BookModel> GetMostPopularBooks(int bookCount)
        {
            return _mapper.Map<IEnumerable<BookModel>>(
                _unitOfWork.HistoryRepository.GetAllWithDetails()
                .GroupBy(b => b.BookId)
                .OrderByDescending(g => g.Count())
                .Select(x => x.First().Book)
                .Take(bookCount));
        }

        public IEnumerable<ReaderActivityModel> GetReadersWhoTookTheMostBooks(int readersCount, DateTime firstDate, DateTime lastDate)
        {

            var readers = _unitOfWork.HistoryRepository.GetAllWithDetails()
                .Where(h => h.TakeDate >= firstDate && h.TakeDate <= lastDate)
                .Select(k => k.Card)
                .GroupBy(c => c.ReaderId)
                .OrderByDescending(r => r.Count())
                .Select(x => x.First().Reader)
                .Take(readersCount)
                .ToList();

            var bookCount = _unitOfWork.HistoryRepository.GetAllWithDetails()
                .Where(h => h.TakeDate >= firstDate && h.TakeDate <= lastDate)
                .Select(k => k.Card)
                .GroupBy(c => c.ReaderId)
                .OrderByDescending(r => r.Count())
                .Select(x => x.Count())
                .Take(readersCount)
                .ToList();

            List<ReaderActivityModel> readerActivities = new List<ReaderActivityModel>();
            foreach(var reader_book in readers.Zip(bookCount, (r, b) => new { ReaderF = r, BookCount = b}))
            {
                ReaderActivityModel ram = new ReaderActivityModel()
                {
                    ReaderId = reader_book.ReaderF.Id,
                    ReaderName = reader_book.ReaderF.Name,
                    BooksCount = reader_book.BookCount
                };
                readerActivities.Add(ram);
            }
            return readerActivities;
        }
    }
}
