﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class CardService : ICardService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly Mapper _mapper;

        public CardService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(CardModel model)
        {
            await _unitOfWork.CardRepository.AddAsync(_mapper.Map<Card>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.CardRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<CardModel> GetAll()
        {
            return _mapper.Map<IEnumerable<CardModel>>(_unitOfWork.CardRepository.FindAllWithDetails());
        }

        public IEnumerable<BookModel> GetBooksByCardId(int cardId)
        {
            return _mapper.Map<IEnumerable<BookModel>>(
                _unitOfWork.HistoryRepository.GetAllWithDetails()
                .Where(h => h.CardId == cardId)
                .Select(b => b.Book));
        }

        public async Task<CardModel> GetByIdAsync(int id)
        {
            return _mapper.Map<CardModel>(await _unitOfWork.CardRepository.GetByIdWithDetailsAsync(id));
        }

        public async Task HandOverBookAsync(int cartId, int bookId)
        {
            History currentHistory = _unitOfWork.HistoryRepository.FindAll()
                .Where(h => h.BookId == bookId && h.CardId == cartId)
                .FirstOrDefault();

            if(currentHistory == null)
            {
                throw new LibraryException(nameof(currentHistory));
            }
            if (currentHistory.ReturnDate != default(DateTime))
            {
                throw new LibraryException(nameof(currentHistory.ReturnDate));
            }

            currentHistory.ReturnDate = DateTime.Now;
            _unitOfWork.HistoryRepository.Update(currentHistory);
            await _unitOfWork.SaveAsync();
        }

        public async Task TakeBookAsync(int cartId, int bookId)
        {
            Book currentBook = await _unitOfWork.BookRepository.GetByIdAsync(bookId);
            Card currentCard = await _unitOfWork.CardRepository.GetByIdAsync(cartId);
            if(currentBook == null)
            {
                throw new LibraryException(nameof(currentBook));
            }
            if(currentCard == null)
            {
                throw new LibraryException(nameof(currentCard));
            }
            if (_unitOfWork.HistoryRepository.FindAll().FirstOrDefault(h => h.CardId == cartId && h.BookId == bookId) != null)
            {
                throw new LibraryException(nameof(currentBook));
            }

            History currentHistory = new History()
            {
                BookId = bookId,
                CardId = cartId,
                Book = currentBook,
                Card = currentCard,
                TakeDate = DateTime.Now
            };

            await _unitOfWork.HistoryRepository.AddAsync(currentHistory);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(CardModel model)
        {
            _unitOfWork.CardRepository.Update(_mapper.Map<Card>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
