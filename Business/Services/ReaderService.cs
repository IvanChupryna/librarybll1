﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data;
using Data.Interfaces;
using System;
using System.Linq;
using Data.Entities;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Business.Validation;

namespace Business.Services
{
    public class ReaderService : IReaderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Mapper _mapper;
        
        public ReaderService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(ReaderModel model)
        {
            Validation(model);
            await _unitOfWork.ReaderRepository.AddAsync(_mapper.Map<Reader>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.ReaderRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<ReaderModel> GetAll()
        {
            return _mapper.Map<IEnumerable<ReaderModel>>(_unitOfWork.ReaderRepository.GetAllWithDetails());
        }

        public async Task<ReaderModel> GetByIdAsync(int id)
        {
            return _mapper.Map<ReaderModel>(await _unitOfWork.ReaderRepository.GetByIdWithDetails(id));
        }

        public IEnumerable<ReaderModel> GetReadersThatDontReturnBooks()
        {
            var cardsIdsWithUnreturnedBooks = _unitOfWork.HistoryRepository.FindAll()
                .Where(h => h.ReturnDate == default(DateTime))
                .Select(x => x.CardId);

            var readersIdsWithUnreturnedBooks = _unitOfWork.CardRepository.FindAll()
                .Where(c => cardsIdsWithUnreturnedBooks.Any(i => i == c.Id))
                .Select(k => k.ReaderId)
                .ToList();

            return _mapper.Map<IEnumerable<ReaderModel>>(_unitOfWork.ReaderRepository.GetAllWithDetails()
                .Where(r => readersIdsWithUnreturnedBooks.Any(c => c == r.Id)));
        }

        public async Task UpdateAsync(ReaderModel model)
        {
            Validation(model);
            _unitOfWork.ReaderRepository.Update(_mapper.Map<Reader>(model));
            await _unitOfWork.SaveAsync();
        }

        private void Validation(ReaderModel model)
        {
            if (model.Name == "")
            {
                throw new LibraryException(nameof(model.Name));
            }
            if (model.Email == "")
            {
                throw new LibraryException(nameof(model.Email));
            }
            if (model.Phone == "")
            {
                throw new LibraryException(nameof(model.Phone));
            }
            if (model.Address == "")
            {
                throw new LibraryException(nameof(model.Address));
            }
        }
    }
}
