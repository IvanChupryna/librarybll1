﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Mapper _mapper;

        public BookService(IUnitOfWork unitOfWork, Mapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(BookModel model)
        {
            Validation(model);
            await _unitOfWork.BookRepository.AddAsync(_mapper.Map<Book>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.BookRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<BookModel> GetAll()
        {
            return _mapper.Map<IEnumerable<BookModel>>(_unitOfWork.BookRepository.FindAllWithDetails());
        }

        public IEnumerable<BookModel> GetByFilter(FilterSearchModel filterSearch)
        {
            return _mapper.Map<IEnumerable<BookModel>>(_unitOfWork.BookRepository.FindAllWithDetails()
                .Where(b => b.Author == filterSearch.Author || b.Year == filterSearch.Year));
        }

        public async Task<BookModel> GetByIdAsync(int id)
        {
            return _mapper.Map<BookModel>(await _unitOfWork.BookRepository.GetByIdWithDetailsAsync(id));
        }

        public async Task UpdateAsync(BookModel model)
        {
            Validation(model);
            _unitOfWork.BookRepository.Update(_mapper.Map<Book>(model));
            await _unitOfWork.SaveAsync();
        }

        private void Validation(BookModel model)
        {
            if (model.Author == "")
            {
                throw new LibraryException(nameof(model.Author));
            }
            if (model.Title == "")
            {
                throw new LibraryException(nameof(model.Title));
            }
            if (model.Year > DateTime.Now.Year)
            {
                throw new LibraryException(nameof(model.Year));
            }
        }
    }
}
